package steps;

import org.openqa.selenium.WebDriver;

public class FirstStep {

    private WebDriver driver;

    public FirstStep(WebDriver driver){
        this.driver = driver;
    }

    protected WebDriver getDriver() {
        return driver;
    }
}
