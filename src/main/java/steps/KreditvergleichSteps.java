package steps;

import org.openqa.selenium.WebDriver;
import pages.smava.KreditvergleichPage;

public class KreditvergleichSteps extends FirstStep {

    private KreditvergleichPage kreditvergleichPage = new KreditvergleichPage(getDriver());

    public KreditvergleichSteps(WebDriver driver){
        super(driver);
    }

    public void chooseVerwendung(){
        kreditvergleichPage.clickWohnenLink();
    }

    public void chooseNettokreditbetrag(){
        kreditvergleichPage.click2750evro();
    }

    public void chooseKreditlaufzeit(){
        kreditvergleichPage.click24Monate();
    }

    public void clickOnVergleichButton(){
        kreditvergleichPage.clickVergleichButton();
    }

    public void clickOnWeiterButton(){
        kreditvergleichPage.clickWeiterButton();
    }
}
