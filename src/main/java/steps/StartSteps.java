package steps;

import org.openqa.selenium.WebDriver;
import pages.smava.StartPage;

public class StartSteps extends FirstStep{

    private final static String START_URL = "https://www.smava.de";
    private final static String EMAIL_INCORRECT = "anmelden@gmail.com";
    private final static String PASSWORD_INCORRECT = "12345";
    private StartPage startPage = new StartPage(getDriver());

    public StartSteps(WebDriver driver){
        super(driver);
    }

    public StartSteps openStartPage() {
        startPage.getHomePage(START_URL);
        return new StartSteps(getDriver());
    }

    public KreditvergleichSteps clickOnKreditvergleichButton(){
        startPage.clickKreditvergleichButton();
        return new KreditvergleichSteps(getDriver());
    }

    public boolean isDisplayedStartSmavaPage(){
        return startPage.isCorrectURL();
    }

    public void loginWithIncorrectData(){
        startPage.typeDataIntoAnmeldenForm(EMAIL_INCORRECT, PASSWORD_INCORRECT);
        startPage.clickAnmeldenSubmitButton();
    }

    public void clickOnAnmeldenButton(){
        startPage.clickAnmeldenButoon();
    }

    public boolean isDisplayedPasswordVergessenLink(){
        return startPage.isDisplayedPasswordVergessenLink();
    }

}
