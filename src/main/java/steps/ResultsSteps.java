package steps;

import org.openqa.selenium.WebDriver;
import pages.smava.ResultsPage;

public class ResultsSteps extends FirstStep{

    private ResultsPage resultsPage = new ResultsPage(getDriver());

    public ResultsSteps(WebDriver driver){
        super(driver);
    }

    public boolean isLoadedResultsPage(){
        return resultsPage.isDisplayedFirstBackButton();
    }
}
