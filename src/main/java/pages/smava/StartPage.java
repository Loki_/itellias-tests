package pages.smava;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;

public class StartPage extends BasePage {

    private final static String KREDITVERGLEICH_BUTTON_XPATH = "//a[contains(text(), 'Kreditvergleich')]";
    private final static String ANMELDEN_BUTTON_XPATH = "//span[contains(text(), 'Anmelden')]";
    private final static String EMAIL_FIELD_XPATH = "//input[@name='email']";
    private final static String PASSWORD_FIELD_XPATH = "//input[@name='password']";
    private final static String ANMELDEN_SUBMIT_BUTTON_XPATH = "//button[@type='submit']";
    private final static String PASSWORD_VERGESSEN_LINK_XPATH = "//a[contains(text(), 'Passwort vergessen?')]";

    public StartPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = KREDITVERGLEICH_BUTTON_XPATH)
    private WebElement kreditvergleichButton;

    @FindBy(xpath = ANMELDEN_BUTTON_XPATH)
    private WebElement anmeldenButton;

    @FindBy(xpath = EMAIL_FIELD_XPATH)
    private WebElement emailField;

    @FindBy(xpath = PASSWORD_FIELD_XPATH)
    private WebElement passwordField;

    @FindBy(xpath = ANMELDEN_SUBMIT_BUTTON_XPATH)
    private WebElement anmeldenSubmitButton;

    @FindBy(xpath = PASSWORD_VERGESSEN_LINK_XPATH)
    private WebElement passwordVergessenLink;

    public void clickKreditvergleichButton(){
        kreditvergleichButton.click();
    }

    public void clickAnmeldenButoon(){
        anmeldenButton.click();
    }

    public boolean isCorrectURL(){
        return getDriver().getCurrentUrl().contains("https://www.smava.de");
    }

    public StartPage getHomePage(String url) {
        getDriver().get(url);
        waitElement();
        return this;
    }

    public void typeDataIntoAnmeldenForm(String email, String password){
        emailField.clear();
        emailField.sendKeys(email);
        passwordField.clear();
        passwordField.sendKeys(password);
    }

    public void clickAnmeldenSubmitButton(){
        anmeldenSubmitButton.click();
        waitElement();
    }

    public boolean isDisplayedPasswordVergessenLink(){
        return passwordVergessenLink.isDisplayed();
    }

}
