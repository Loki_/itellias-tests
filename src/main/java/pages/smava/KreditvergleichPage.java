package pages.smava;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;

public class KreditvergleichPage extends BasePage {

    private final static String VERWENDUNG_MENU_DROPDOWN_XPATH = "//select[@id='header-form-category']";
    private final static String WOHNEN_LINK_XPATH = "//option[@value='886']";
    private final static String KREDITLAUFZEIT_MENU_DROPDOWN_XPATH = "//select[@id='header-form-duration']";
    private final static String MONATE_24_LINK_XPATH = "//option[@value='24']";
    private final static String NETTOKREDITBETRAG_MENU_DROPDOWN_XPATH = "//select[@id='header-form-amount']";
    private final static String KREDIT_SUM_2750_LINK_XPATH = "//option[@value='2750']";
    private final static String VERGLEICH_BUTTON_XPATH = "//a[@id='header-form-link']";
    private final static String WEITER_BUTTON_XPATH = "//button[@id='cta_btn_0']";

    public KreditvergleichPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = VERWENDUNG_MENU_DROPDOWN_XPATH)
    private WebElement verwendungMenuDropdown;

    @FindBy(xpath = WOHNEN_LINK_XPATH)
    private WebElement wohnenLink;

    @FindBy(xpath = KREDITLAUFZEIT_MENU_DROPDOWN_XPATH)
    private WebElement kreditlaufzeitMenuDropdown;

    @FindBy(xpath = MONATE_24_LINK_XPATH)
    private WebElement monate24Link;

    @FindBy(xpath = NETTOKREDITBETRAG_MENU_DROPDOWN_XPATH)
    private WebElement nettokreditbetragMenuDropdown;

    @FindBy(xpath = KREDIT_SUM_2750_LINK_XPATH)
    private WebElement kreditSum2750Link;

    @FindBy(xpath = VERGLEICH_BUTTON_XPATH)
    private WebElement vergleichButton;

    @FindBy(xpath = WEITER_BUTTON_XPATH)
    private WebElement weiterButton;

    public void clickWohnenLink(){
        verwendungMenuDropdown.click();
        wohnenLink.click();
    }

    public void click24Monate(){
        kreditlaufzeitMenuDropdown.click();
        monate24Link.click();
    }

    public void click2750evro(){
        nettokreditbetragMenuDropdown.click();
        kreditSum2750Link.click();
    }

    public void clickVergleichButton(){
        vergleichButton.click();
    }

    public void clickWeiterButton(){
        weiterButton.click();
    }

}
