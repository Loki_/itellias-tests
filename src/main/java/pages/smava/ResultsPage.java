package pages.smava;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;

public class ResultsPage extends BasePage {

    private final static String FIRST_BACK_BUTTON_XPATH = "//span[@class='form-buttons__label-back']";

    public ResultsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = FIRST_BACK_BUTTON_XPATH)
    private WebElement firstBackButton;

    public boolean isCorrectURL(){
        return getDriver().getCurrentUrl().contains("https://www.smava.de/kreditanfrage/kreditantrag.html");
    }

    public void clickFirstBackButton(){
        firstBackButton.click();
    }

    public boolean isDisplayedFirstBackButton(){
        scrollToElement(firstBackButton);
        return firstBackButton.isDisplayed() && isCorrectURL();
    }
}
