package runner;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.AnmeldenTest;
import tests.AskLoanTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AnmeldenTest.class,
        AskLoanTest.class
})
public class TestsRunner {
}
