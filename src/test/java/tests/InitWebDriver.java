package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InitWebDriver {

    private static final Logger LOG = LoggerFactory.getLogger(InitWebDriver.class);

    @BeforeClass
    public static void getInitDriver() {
        LOG.debug("WebDriver initialization ---------------");
        WebDriverManager.firefoxdriver().setup();
    }

}
