package tests;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class BaseTest extends InitWebDriver {

    protected WebDriver driver;

    @Before
    public void setUp() {
        driver = new FirefoxDriver();
        driver.manage()
                .deleteAllCookies();
        driver.manage()
                .timeouts()
                .pageLoadTimeout(90, TimeUnit.SECONDS);
        driver.manage()
                .timeouts()
                .implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage()
                .window()
                .maximize();
    }

    @After
    public void closeWebDriver() {
        if (driver != null) {
            driver.quit();
        }
    }

}

