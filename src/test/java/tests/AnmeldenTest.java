package tests;

import org.junit.Before;
import org.junit.Test;
import steps.StartSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class AnmeldenTest extends BaseTest {

    private StartSteps startSteps;

    @Before
    public void openStartSmavaPage(){
        startSteps = new StartSteps(driver);
        startSteps.openStartPage();
        assertThat(startSteps.isDisplayedStartSmavaPage(), is(true));
    }

    @Test
    public void verifyThatLoginIsNotEndedIfDataIsIncorrect(){
        startSteps.clickOnAnmeldenButton();
        startSteps.loginWithIncorrectData();
        assertThat(startSteps.isDisplayedPasswordVergessenLink(), is(true));
    }
}
