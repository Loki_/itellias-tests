package tests;

import org.junit.Before;
import org.junit.Test;
import steps.KreditvergleichSteps;
import steps.ResultsSteps;
import steps.StartSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class AskLoanTest extends BaseTest {

    private StartSteps startSteps;
    private KreditvergleichSteps kreditvergleichSteps;
    private ResultsSteps resultsSteps;

    @Before
    public void openStartSmavaPage(){
        startSteps = new StartSteps(driver);
        startSteps.openStartPage();
        assertThat(startSteps.isDisplayedStartSmavaPage(), is(true));
    }

    @Test
    public void verifyThatSecondLoanParameterPageWillLoadAfterFirstIfUserNoAuthorized(){
        kreditvergleichSteps = startSteps.clickOnKreditvergleichButton();
        kreditvergleichSteps.chooseVerwendung();
        kreditvergleichSteps.chooseKreditlaufzeit();
        kreditvergleichSteps.chooseNettokreditbetrag();
        kreditvergleichSteps.clickOnVergleichButton();
        kreditvergleichSteps.clickOnWeiterButton();
        resultsSteps = new ResultsSteps(driver);
        assertThat(resultsSteps.isLoadedResultsPage(), is(true));
    }
}
